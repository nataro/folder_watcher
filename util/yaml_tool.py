from typing import Dict, Tuple, Any, Optional, Sequence, List, Union
import re
import codecs
import yaml

from logging import getLogger, StreamHandler, DEBUG

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)


def yaml_read(yaml_file: str) -> Optional[Dict]:
    """
    Parameters
    ----------
    yaml_file

    Returns
    -------

    """

    if not re.search(r'\.(YAML|YML)$', yaml_file.upper()):
        mes = f'''
        ファイル名の拡張子は「*.yaml」もしくは「*.yml」にしてください。
        現在のファイル名：{yaml_file}
        '''
        logger.error(mes)
        return
    try:
        with codecs.open(yaml_file, 'r', 'utf-8') as yaml_file:
            # data_dic = yaml.load(yaml_file, Loader=yaml.SafeLoader)
            data_dic = yaml.safe_load(yaml_file)
            return data_dic
    except OSError as e:
        logger.error(f"OS error occurred trying to read {yaml_file}")
        logger.error(e)


def yaml_write(data: Union[Dict, List[Dict]], yaml_file: str) -> bool:
    """
    Parameters
    ----------
    data
    yaml_file

    Returns
    -------

    """
    if not re.search(r'\.(YAML|YML)$', yaml_file.upper()):
        mes = f'''
        ファイル名の拡張子は「*.yaml」もしくは「*.yml」にしてください。
        現在のファイル名：{yaml_file}
        '''
        logger.error(mes)
        return False
    try:
        with codecs.open(yaml_file, 'w', 'utf-8') as yaml_file:
            # default_flow_style=False でブロックスタイルで出力
            # yaml.dump(data, yaml_file, encoding='utf-8', allow_unicode=True, default_flow_style=False)
            yaml.safe_dump(data, yaml_file, encoding='utf-8', allow_unicode=True, default_flow_style=False)
            return True
    except OSError as e:
        logger.error(f"OS error occurred trying to write {yaml_file}")
        logger.error(e)


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    data = yaml_read('test.yaml')
    # data = yaml_read('test.txe')
    print(data)

    dic_data = {
        'fuga.csv': {'first': 1000, 'end': 9999},
        'hoge.csv': {'first': 100, 'end': 999},
    }

    print(yaml_write(dic_data, 'test.yaml'))
