import logging

from logging import getLogger, Formatter, StreamHandler, FileHandler, DEBUG, ERROR

from typing import Optional, Sequence

'''
https://own-search-and-study.xyz/2019/10/20/python-logging-clear/
'''


def get_logger_with_presets(
        logger_name: str,
        log_file: Optional[str] = None,
        format: Optional[str] = None,
        date_format: Optional[str] = None,
        level: int = DEBUG,
) -> logging.Logger:
    """
    log_fileが設定されていた場合はFileHandlerに出力

    :param logger_name:
    :param log_file:
    :param format:
    :param date_format:
    :param level:
    :return: 設定済みのloggerを返す
    """
    logger = getLogger(logger_name)
    logger.setLevel(level)

    # 一旦設定済みのハンドラを削除する
    kill_all_handlers(logger)

    formatter = Formatter(fmt=format, datefmt=date_format)
    # formatter = None if format is None else Formatter(format)
    hd = StreamHandler() if log_file is None else FileHandler(log_file)
    hd.setFormatter(formatter)
    logger.addHandler(hd)
    return logger


def kill_logger(logger: logging.Logger):
    """
    指定したloggerを削除する
    :param logger:
    :return:
    """
    name = logger.name
    del logging.Logger.manager.loggerDict[name]


def kill_all_handlers(logger: logging.Logger):
    """
    loggerに設定されているhandlerを全て削除する
    :param logger:
    :return:
    """
    for h in logger.handlers:
        logger.removeHandler(h)


def kill_handler(logger: logging.Logger, handlers: Sequence[logging.Handler]):
    """
    loggerから特定のハンドルを削除する
    :param logger:
    :param handlers:
    :return:
    """
    for handler in handlers:
        logger.removeHandler(handler)