from typing import Dict, Tuple, Any, Optional, Sequence, List, Union, Iterable, AnyStr
import re
import codecs
import csv
import datetime
import pathlib
from logging import getLogger, StreamHandler, DEBUG
from itertools import islice, tee
from collections import defaultdict

from util.yaml_tool import yaml_write, yaml_read

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)


class CsvAnalyzer(object):
    """
    指定したファイルを読み込んで、CSVファイルとして解析する
    解析した情報（ファイル名や日付など）をself._state_yamlで指定したyamlファイルに保存
    """

    def __init__(
            self,
            skip_rows_num: Optional[int],
            codec: AnyStr = 'ms932',
            csv_date_key: AnyStr = 'date',
            csv_date_format: AnyStr = '%Y-%m-%d %H:%M:%S',
            yaml_file: AnyStr = 'state.yaml',
            yaml_data_key: AnyStr = 'last',
    ):
        """
        Parameters
        ----------
        skip_rows_num : 読み込んだcsvファイルの先頭からskip_rows_numまでの行を無視する
        codec : 解析するcsvファイルのcodec
        csv_date_key : 解析対象となるcsvファイルのカラムの中で、日付情報を保持ししているカラムのkey
        csv_date_format : 文字列を日付に変化する際のフォーマット
        yaml_file : 解析情報を保持しておくyamlファイルのファイル名
        yaml_data_key : 最終要素の日付解析情報をyamlに保持しておく際のkey
        """
        self._skip_row = skip_rows_num
        self._codec = codec
        self._csv_date_key = csv_date_key
        self._date_format = csv_date_format
        self._yaml_file = yaml_file
        self._yaml_date_key = yaml_data_key
        self._csv_file_size = defaultdict(int)  # プログラム開始時に毎回初期値[0]になるdefaultdictを生成

    @property
    def csv_data_key(self):
        """getterのみの省略表記"""
        return self._csv_date_key

    @staticmethod
    def csv_file_name_check(csv_file: AnyStr) -> bool:
        if not re.search(r'\.(CSV|TXT)$', csv_file.upper()):
            mes = '''
            ファイル名の拡張子は「.csv」もしくは「.txtにしてください。
            現在のファイル名：{0}
            '''.format(csv_file)
            logger.error(mes)
            return False
        return True

    @staticmethod
    def is_latter_date(
            date: AnyStr,
            date_thresh: Optional[AnyStr] = '2012-12-29 13:49:37',
            date_format: AnyStr = '%Y-%m-%d %H:%M:%S',
    ) -> Optional[bool]:
        """
        文字列を日時に変換して閾値より以降かどうかを確認する

        Parameters
        ----------
        date : 評価対象となる日付の文字列
        date_thresh : 基準となる閾値（日付の文字列）
        date_format : 文字列を日時に変換する際のフォーマット

        Returns
        -------
            bool : 基準より後の日付->True(基準がNoneの場合全てTrue)
            None : 日付変換などでエラーとなった場合
        """
        try:
            if date_thresh is None:
                return True
            dt = datetime.datetime.strptime(date, date_format)
            dt_thresh = datetime.datetime.strptime(date_thresh, date_format)
            return True if dt > dt_thresh else False
        except ValueError as e:
            logger.error(e)

    def get_yaml_state(self, file_key: AnyStr, date_key: AnyStr):
        """
        対象となるyamlファイルはself._yaml_file

        yaml_read()でyamlから取り出したデータのイメージ
        yaml_dict ={
                'test.csv': {'last': '2021-4-10 13:49:37'},
                'in1.csv': {'last': '2021-4-12 13:49:37'},
        }

        Parameters
        ----------
        file_key : yamlファイルに格納してあるデータをkey（上記例の'csvのファイル名'）で指定
        date_key : yamlファイルに格納してあるcsvファイルに関する情報をkey(上記例の'last')で指定
        """
        try:
            d_y = yaml_read(self._yaml_file)
            if d_y is not None:
                state = d_y.get(file_key, None)
                if state is not None:
                    return state.get(date_key, None)
        except AttributeError as e:
            logger.error(e)

    def set_yaml_state(self, file_key: AnyStr, date_key: AnyStr, date: AnyStr):
        try:
            state_dict = {str(file_key): {str(date_key): str(date)}}
            return yaml_write(state_dict, self._yaml_file)
        except AttributeError as e:
            logger.error(e)

    def latter_elements_picker(
            self,
            ite_dicts: Iterable[Dict],
            dict_key: AnyStr,
            date_thresh: Optional[AnyStr],
            date_format: AnyStr
    ) -> Optional[List[Dict]]:
        """
        dictが取り出せるterableな要素の中身を確認し、dict[dict_key]に格納されている日時（文字）
        が閾値以降になっているdictをListに格納して返す。

        Parameters
        ----------
        ite_dicts : iterableな要素（dictが取り出せる）
        dict_key : dictからvalue(日付の文字列)を取り出す際のkey
        date_thresh : 基準となる閾値（日付の文字列）
        date_format : 文字列を日時に変換する際のフォーマット

        Returns
        -------
           List[dict] : 閾値以降という条件に合致したもの
           None : エラーが発生した場合や該当する要素が無い場合
        """
        picked = []

        for d in ite_dicts:
            try:
                d_val = d.get(dict_key, None)
                if d_val is None:
                    raise AttributeError(f"Can not find the dict key '{dict_key}'.")
                if self.is_latter_date(d_val, date_thresh, date_format):
                    picked.append(d)
            except AttributeError as e:
                logger.error(e)
        return None if not picked else picked

    # def csv_reader_check(
    #         self,
    #         ite_dicts: Iterable[Dict],
    #         dict_key: AnyStr,
    #         date_thresh: Optional[AnyStr],
    #         date_format: AnyStr
    # ) -> Optional[List[Dict]]:
    #     """
    #     dictが取り出せるterableの最後のdict要素を取り出し、dict[dict_key]が閾値以降に
    #     なっているか確認する。
    #     閾値以降になっていた場合は、self.latter_elements_picker()を用いて、該当する要素を全
    #     て取り出して返り値とする。
    #
    #     Parameters
    #     ----------
    #     ite_dicts : iterableな要素（dictが取り出せる）
    #     dict_key : dictからvalue(日付の文字列)を取り出す際のkey
    #     date_thresh : 基準となる閾値（日付の文字列）
    #     date_format : 文字列を日時に変換する際のフォーマット
    #
    #     Returns
    #     -------
    #         None : dict[dict_key]が閾値以降になっていない場合やエラーが起きた場合
    #         List[dict] : 閾値以降の該当する要素
    #     """
    #     try:
    #
    #         # ite_dictsを2回使いたいので複製しておく
    #         it1, it2 = tee(ite_dicts, 2)  # 第二引数では複製するiteratorの個数(省略時は2)
    #         # iteratorの最後の要素のみを取得
    #         *_, last = it1
    #         d_val = last.get(dict_key, None)
    #         # 最後の要素が保持している日付以降になっているか確認
    #         if self.is_latter_date(d_val, date_thresh, date_format):
    #             return self.latter_elements_picker(it2, dict_key, date_thresh, date_format)
    #     except Exception as e:
    #         logger.error(e)

    def get_new_elements(self, csv_file: AnyStr) -> Optional[List[Dict]]:
        """
        解析対象となるcsvファイル名を渡し、yamlファイルに保持している日付情報以降の要素（辞書型）があるかを確認。
        新しい要素（辞書型）があれば、その要素をlistに格納して返す。

        get_new_elements()で新しい要素を取得した後、その返り値の情報を用いて、update_yaml_state()でyaml
        ファイルに格納している情報を更新する必要がある。

        ファイルが更新されているか確認する方法
        [１．イテレータの最後の要素の日付を確認する]
            # ite_dictsを2回使いたいので複製しておく
            it1, it2 = tee(ite_dicts, 2)  # 第二引数では複製するiteratorの個数(省略時は2)
            # iteratorの最後の要素のみを取得
            *_, last = it1
            d_val = last.get(dict_key, None)
            # 最後の要素が保持している日付以降になっているか確認
            if self.is_latter_date(d_val, date_thresh, date_format):
                return self.latter_elements_picker(it2, dict_key, date_thresh, date_format)

        [２．ファイルサイズを確認する]
            事前時ファイルサイズを確認（今回はこちらを採用）

        Parameters
        ----------
        csv_file : 解析対象となるcsvファイルのファイル名

        Returns
        -------
        Optional[List[Dict]] :
            csvファイルを解析して得られた新しい要素（辞書型）を格納したList
            新しい要素が無い場合はNoneを返す
        """
        if not self.csv_file_name_check(csv_file):
            return

        try:
            # 保持されているファイルサイズ情報と一致する場合は何もしないで終了
            before_size = self._csv_file_size[csv_file]
            current_size = pathlib.Path(csv_file).stat().st_size
            if before_size == current_size:
                return
            # 保持しているファイルサイズを更新
            self._csv_file_size[csv_file] = current_size

            # yamlに保存されている日付を確認（該当する情報が無い場合はNoneが返される）
            yaml_date_str = self.get_yaml_state(csv_file, self._yaml_date_key)
            # csvファイルの解析を行う
            with codecs.open(csv_file, 'r', self._codec, errors="") as cf:
                if self._skip_row is not None:
                    # iterableな要素をhoge[num:]みたいにスライス
                    cf = islice(cf, self._skip_row, None)
                # csv_reader = csv.DictReader(cf,
                #                             delimiter=",",
                #                             doublequote=True,
                #                             lineterminator="\r\n",
                #                             quotechar='"',
                #                             skipinitialspace=True)
                csv_reader = csv.DictReader(cf)

                # picked = self.csv_reader_check(csv_reader, self._csv_date_key, yaml_date_str, self._date_format)
                picked = self.latter_elements_picker(csv_reader, self._csv_date_key, yaml_date_str, self._date_format)

                return picked

        except FileNotFoundError:
            logger.error(f"{csv_file} can not be found ...")
        except OSError as e:
            logger.error(f"OS error occurred trying to read {csv_file}")
            logger.error(e)

    def update_yaml_state(self, csv_file: AnyStr, data_dict: Dict):
        """
        csvファイルを解析して得られる

        data_dict = {
            'date' : '2021-4-10 13:49:37',
            'hoge' : 23.5,
            'fuga' : 999,
            ...
        }

        というような辞書から、self._csv_date_keyで指定したkeyのvalueを取り出し、
        yamlファイルに保存する。

        yamlファイルに保存する際には、csv_file名をkeyとした辞書型に、日付に関する情報を
        self._yaml_date_keyをkeyとした辞書型として格納する。

        Parameters
        ----------
        csv_file : csv_file名（yamlに保存する際にkeyとして利用）
        data_dict : 上記csv解析で得られた辞書型
        """
        try:
            d_val = data_dict.get(self._csv_date_key, None)
            if d_val is not None:
                self.set_yaml_state(csv_file, self._yaml_date_key, d_val)
        except AttributeError as e:
            logger.error(e)


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    print('test')

    dir_path = './data.csv'

    ca = CsvAnalyzer(skip_rows_num=0)

    print('test')
    elems = ca.get_new_elements(dir_path)
    print(elems)
