from typing import Dict, Optional, List,  AnyStr

import contextlib
from logging import getLogger, StreamHandler, DEBUG

import sqlite3

from util.yaml_tool import yaml_write, yaml_read

logger = getLogger(__name__)
logger.setLevel(DEBUG)
handler = StreamHandler()
logger.addHandler(handler)



class SqliteAnalyzer(object):
    """
    SQLデータベース解析用のクラス

    変更する必要性が少ない引数はインスタンス変数として受ける
    ・yamlファイル名
    ・解析対象となるカラム名
    使用時に変更する可能性が高い引数は、関数の引数として与える
    ・データベースファイル名
    ・テーブル名（これに関しては引数を与えなくても実行できるようにしておく）

    ＊本クラスはデータ解析、取得が目的のクラスなので基本的にリードオンリーでget_new_elements()する。
    ＊yamlファイルの書き換えなどは、必要時にユーザー側で別途実行すること
    """

    def __init__(
            self,
            key_column: AnyStr = 'No',
            codec: AnyStr = 'CP932',
            yaml_file: AnyStr = 'main_wd_state.yaml',
    ):
        self._decode_codec = codec
        self._key_column = key_column
        self._yaml_file = yaml_file

    @property
    def sql_data_key(self):
        """getterのみの省略表記"""
        return self._key_column

    def get_yaml_state(self, file_key: AnyStr, target: AnyStr):
        """
        対象となるyamlファイルはself._yaml_file

        yaml_read()でyamlから取り出したデータのイメージ
        yaml_dict ={
                'test.db': {'No': 24478},
        }

        Parameters
        ----------
        file_key : yamlファイルに格納してあるデータをkey（上記例の'データベースファイル名'）で指定
        target : yamlファイルに格納してあるデータベースファイルに関する情報をカラム名(上記例の'No')で指定
        """
        try:
            d_y = yaml_read(self._yaml_file)
            if d_y is not None:
                state = d_y.get(file_key, None)
                if state is not None:
                    return state.get(target, None)
        except AttributeError as e:
            logger.error(e)

    def set_yaml_state(self, file_key: AnyStr, target: AnyStr, data):
        """

        Parameters
        ----------
        file_key : yamlファイルに格納してあるデータをkey（上記例の'データベースファイル名'）で指定
        target : yamlファイルに格納してあるデータベースファイルに関する情報をカラム名(上記例の'No')で指定
        data : 実際に書き込みたい情報
        """
        try:
            state_dict = {str(file_key): {str(target): data}}
            return yaml_write(state_dict, self._yaml_file)
        except AttributeError as e:
            logger.error(e)

    def get_new_elements(self, sql_file: AnyStr, table: Optional[AnyStr] = None) -> Optional[List[Dict]]:
        """
         ・テーブルを丸ごとコピーしておけば、sqlで差分のみ簡単に取り出せるが、sql以外のデータ（csvなど）には使えないので不採用
         ・'select count(*) from table_name'でレコード数を確認して、更新の有無を確認する方法もあるが今回は不採用
         (↑同じidのものを複数回送信してしまう可能性があるため)

         解析対象となるファイル名を渡し、yamlファイルに保持している日付情報以降の要素（辞書型）があるかを確認。
         新しい要素（辞書型）があれば、その要素をlistに格納して返す。

         get_new_elements()で新しい要素を取得した後、その返り値の情報を用いて、update_yaml_state()で
         別途yamlファイルに格納している情報を更新する必要がある。

         Parameters
         ----------
         sql_file : 解析対象となるcsvファイルのファイル名
         table : 解析対象となるテーブル名を指定。Noneの場合は、検出した一番最初のテーブルを対象とする

         Returns
         -------
         Optional[List[Dict]] :
             ファイルを解析して得られた新しい要素（辞書型）を格納したList
             新しい要素が無い場合はNoneを返す
             （新旧比較の解析対象はself._key_columnで指定されているカラムのデータ）
         """

        # yamlに保存されている更新履歴を確認（該当する情報が無い場合はNoneが返される）
        yaml_date = self.get_yaml_state(sql_file, self._key_column)

        logger.debug(f"yaml state: {type(yaml_date)} , {yaml_date}.")

        try:
            with contextlib.closing(sqlite3.connect(sql_file)) as conn:
                if self._decode_codec is not None:
                    # こうしないと日本語をエラーで開けない場合がある('Shift_JIS')
                    conn.text_factory = lambda b: b.decode(self._decode_codec)

                with contextlib.closing(conn.cursor()) as cursor:

                    # 対象のテーブルを指定
                    if table is None:
                        # テーブルの一覧を取得
                        elem = cursor.execute("select name from sqlite_master where type='table'")
                        table_tup = next(elem)
                        # リスト中の先頭のテーブル取得
                        table = '' if not table_tup else table_tup[0]

                    # テーブルのレコード数を確認してみる
                    query = f"select count(*) from {table}"
                    row_count = cursor.execute(query)
                    logger.debug(f"total record num : {next(row_count)}")

                    # 指定したテーブルのカラム情報を取得
                    """
                    [指定したテーブルのカラム情報を取得] 
                    column = cursor.execute(f"PRAGMA table_info({table_name})")
                    for i in column:
                        print(i)

                    下記内容がタプルでカラム毎に出力される
                    cid         name      type      notnull     dflt_value  pk
                    インデックス  カラム名    データ型   nullの可否  カラムの初期値  主キーかどうか 
                    (0, 'x', '', 0, None, 0)
                    """
                    column_ite = cursor.execute(f"PRAGMA table_info({table})")
                    column_keys = [c[1] for c in column_ite]

                    # 新しいデータのみを取り出す
                    query = f"select * from {table}"
                    if yaml_date is not None:
                        query += f" where {self._key_column}>{yaml_date}"
                    row_ite = cursor.execute(query)

                    # カラムに対応する各レコードを辞書型のデータにし、リストに格納
                    picked = [{key: val for key, val in zip(column_keys, values)} for values in row_ite]

                    logger.debug(f"picked record len : {len(picked)}")

                    return picked

        except FileNotFoundError:
            logger.error(f"{sql_file} can not be found ...")
        except sqlite3.Error as e:
            logger.error(f"OS error occurred trying to read {sql_file}")
            logger.error(e)

    def update_yaml_state(self, sql_file: AnyStr, data_dict: Dict):
        """
        data_dict = {
            'date' : '2021-4-10 13:49:37',
            'hoge' : 23.5,
            'fuga' : 999,
            ...
        }

        というような辞書から、self._key_columnで指定したkeyのvalueを取り出し、
        yamlファイルに保存する。

        yamlファイルに保存する際には、sql_file名をkeyとした辞書型に、self._key_columnで
        指定されたカラムに関する情報をself._yaml_date_keyをkeyとした辞書型として格納する。

        yamlに保存されるデータの例
        ./MeasData.db:
          'No': 4

        Parameters
        ----------
        sql_file : sqliteデータファイル名（yamlに保存する際にkeyとして利用）
        data_dict : self.get_new_elements()などで得られた辞書型
        """
        try:
            d_val = data_dict.get(self._key_column, None)
            if d_val is not None:
                self.set_yaml_state(sql_file, self._key_column, d_val)
        except AttributeError as e:
            logger.error(e)


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    print('test')

    db_name = 'MeasData.db'
    table_name = 'AlcoholMeasData'
    key_column = 'No'

    sa = SqliteAnalyzer(key_column=key_column)

    print('test')
    # data = sa.get_new_elements(sql_file=db_name, table=table_name)
    data = sa.get_new_elements(sql_file=db_name)
    print(data[0])

