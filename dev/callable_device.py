from abc import ABCMeta, abstractmethod


class CallableDevice(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, **kwargs):
        """Retrieve data from the input source and return an object."""
        return

    @abstractmethod
    def open(self):
        """Open the device."""
        raise NotImplementedError()

    @abstractmethod
    def close(self):
        """Close the device.
        """
        raise NotImplementedError()
