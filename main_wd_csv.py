import time
from typing import List, AnyStr, Dict
from logging import getLogger, StreamHandler, FileHandler, DEBUG
import json

import click
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from dev.xbee_router import XbeeSenderDevice
from th.xbee_director import XbeeHandlingBuilder, XbeeHandlingDirector
from util.csv_tool import CsvAnalyzer

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
# # handler = FileHandler('dousa_log.txt')
# handler.setLevel(DEBUG) # 出力するログのレベルを設定(デフォルトはNOTSET)
logger.addHandler(handler)


class MyWatchdogHandler(PatternMatchingEventHandler):
    def __init__(self,
                 patterns: List[AnyStr],
                 xbee_dr: XbeeHandlingDirector = None,
                 csv_ana: CsvAnalyzer = None,
                 ):
        super().__init__(patterns=patterns)
        # xbee関連に作成したクラスがthreadを使っているのでwithでshutdownさせたい
        self._xbee_director = xbee_dr
        self._csv_analyzer = csv_ana

    def post_via_xbee(self, data: Dict):
        '''
        jsonで送ると送信制限容量を超えてしまうので、valueのみを','で連結した文字列としてxbeeで送信
        '''
        print(data)
        value_list = list(data.values())
        data_str = ','.join(map(str, value_list))
        data_str = data_str[:100]
        # print(data_str)
        # print(len(data_str.encode('utf-8')))
        self._xbee_director(data_str)

    def post_via_xbee_segmented_send(self, dic_data: Dict, interval_msec: int = 500):
        """
        xbeeの送信容量制限を回避するために、self._csv_analyzer.csv_data_keyをキーにして、
        各要素を個別に送信する。

        keyが「'測定結果'」の場合
        dict_data = {'測定結果':'2015/10/02 16:10', 'サンプルNo.', "'019-015", ...}
        分割送信データの例('サンプルNo.'を分割送信している)
        {'2015/10/02 16:10': {'サンプルNo.', "'019-015"}}

        Parameters
        ----------
        dic_data : 送信する元データ
        interval_msec :  分割送信する際の時間間隔
        """
        head_key = self._csv_analyzer.csv_data_key
        head_val = dic_data.get(head_key)
        if head_val is None:
            logger.info(f"The key [{head_key}] is not include in the sent dict.")
            return
        for k, v in dic_data.items():
            if k is not head_key:
                sent_dic = {head_val: {k: v}}
                sent_data = json.dumps(sent_dic)
                logger.debug(f"send: {sent_data}")
                self._xbee_director(sent_data)
                # 送信間隔を設けておく
                time.sleep(interval_msec / 1000)

    def check_and_send(self, csv_file):
        new_elements = self._csv_analyzer.get_new_elements(csv_file)

        if new_elements is not None:
            # do something
            for d in new_elements:
                # logger.debug(f"send: {d}")
                # self.post_via_xbee(d)
                self.post_via_xbee_segmented_send(d)

            # 最後の要素の日時情報でyamlファイルを更新
            last_elem = new_elements[-1]
            self._csv_analyzer.update_yaml_state(csv_file, last_elem)

    def on_modified(self, event):
        """
        PatternMatchingEventHandlerメソッドのOverRide
        """
        logger.debug(f"{event.src_path} is modified.")
        self.check_and_send(event.src_path)

    def on_moved(self, event):
        logger.debug(f"{event.src_path} is moved.")

    def on_created(self, event):
        logger.debug(f"{event.src_path} is created.")

    def on_deleted(self, event):
        logger.debug(f"{event.src_path} is deleted.")

    def __enter__(self):
        """
        with 文開始時にコールされる。
        ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
        """
        self._xbee_director.device_open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
        例外が発生しないときはすべて None がセットされる
        """
        self._xbee_director.device_close()
        self._xbee_director.executor_shutdown()


def folder_watch(path, extension, xbee_dr, csv_ana):
    with MyWatchdogHandler(["*" + extension], xbee_dr, csv_ana) as hd:
        observer = Observer()
        observer.schedule(hd, path, recursive=False)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()


# target_path = 'C:/Program Files/Core Temp'
# csv_key = '測定日時'
# date_format = '%Y/%m/%d %H:%M'

@click.command()
@click.option('--path', '-t', default='./', type=click.Path(exists=True))
@click.option('--extension', '-e', default='.csv')
@click.option('--port', '-p', default='COM1')
@click.option('--baud', '-b', default=9600, type=int)
@click.option('--csv_key', '-k', default="date")
@click.option('--d_format', '-f', default="%Y-%m-%d %H:%M:%S")
@click.option('--skip', '-s', default=0, type=int)
def cmd(path, extension, port, baud, skip, csv_key, d_format):
    xb_dev = XbeeSenderDevice(port, baud)
    xb_hb = XbeeHandlingBuilder(xb_dev)
    xb_dc = XbeeHandlingDirector(xb_hb)

    logger.debug(d_format)

    ca = CsvAnalyzer(
        skip_rows_num=skip,
        csv_date_key=csv_key,
        csv_date_format=d_format,
    )
    folder_watch(path, extension, xb_dc, ca)


def main():
    cmd()


if __name__ == '__main__':
    '''
    スペース入りの文字列を渡して実行する方法
    python main_wd.py -p COM7 -k 測定日時 -f "%Y/%m/%d %H:%M"
    '''
    main()
