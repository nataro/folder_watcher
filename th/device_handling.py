from logging import getLogger, StreamHandler

import threading
from concurrent.futures import ThreadPoolExecutor

from abc import ABCMeta, abstractmethod
from typing import Sequence, Optional, Dict

from dev.callable_device import CallableDevice

logger = getLogger(__name__)
handler = StreamHandler()
logger.addHandler(handler)


class DeviceHandlingBuilderNonThread(metaclass=ABCMeta):
    def __init__(self, device: CallableDevice, max_workers: int = 3):
        self._device = device

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    # def executor_shutdown(self) -> None:
    #     pass
    #
    # def handling_thread(self, **kwargs):
    #     pass

    @abstractmethod
    def handle_via_device(self,
                          # event: threading.Event,
                          # lock: threading.Lock,
                          **kwargs, ):
        pass


class DeviceHandlingBuilder(metaclass=ABCMeta):
    """
    ここを参考に
    https://qiita.com/__init__/items/74b36eba31ccbc0364ed

    一度動き出したスレッドを途中で停止させる方法
    https://stackoverflow.com/questions/29177490/how-do-you-kill-futures-once-they-have-started

    """

    def __init__(self, device: CallableDevice, max_workers: int = 3):
        self._device = device
        self._executor = ThreadPoolExecutor(max_workers=max_workers)
        self._lock = threading.Lock()  # 複数のスレッドが同時にデバイスにアクセスしないようにロックする

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    def executor_shutdown(self) -> None:
        self._executor.shutdown()  # ThreadPoolを閉じる

    def handling_thread(self, **kwargs):
        """

        Parameters
        ----------
        kwargs
            キーワード引数で受け取ったkwargsはDict型になる
        -------

        """
        # self._executor.submit(self.handle_via_device, self._lock, kwargs)
        # 辞書型であるkwargsを、キーワード引数として渡したいので**kwargsとする
        self._executor.submit(self.handle_via_device, self._lock, **kwargs)

    @abstractmethod
    def handle_via_device(self,
                          # event: threading.Event,
                          lock: threading.Lock,
                          **kwargs, ):
        pass


class DeviceHandlingBuilderReceive(DeviceHandlingBuilder):
    """
    thredを実行する際に辞書型などで情報を渡して、その情報をthred処理内で利用する場合、
    受け取った辞書型の内容についてバリデーションを行うための関数を追加したもの。
    """

    @abstractmethod
    def check_dict(self, received: Dict) -> str:
        pass

    @staticmethod
    def keys_check(target_keys: Sequence, must_keys: Sequence) -> bool:
        """
        target_keysの中に、must_keysが全部含まれているか確認する
        """
        return set(target_keys) >= set(must_keys)
