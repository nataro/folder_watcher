import json
from typing import Optional, Dict, AnyStr

from logging import getLogger, StreamHandler, DEBUG
import threading

from th.device_handling import DeviceHandlingBuilder
from dev.xbee_router import XbeeSenderDevice

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)


class XbeeHandlingBuilder(DeviceHandlingBuilder):
    def __init__(self, device: XbeeSenderDevice):
        super().__init__(device)

    def handle_via_device(self,
                          lock: threading.Lock,
                          send_str: Optional[AnyStr] = None,
                          dst_address: Optional[AnyStr] = None,
                          ):
        """
        DeviceHandlingBuilder.hadling_thred(self, **kwargs)において以下のように呼び出される
            # 辞書型であるkwargsを、キーワード引数として渡したいので**kwargsとする
            self._executor.submit(self.handle_via_device, self._lock, **kwargs)
        """
        logger.debug(self.__class__.__name__ + ' : thread start')
        logger.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logger.debug(self.__class__.__name__ + ' : lock')
            for i in range(3):  #再試行の回数
                # is_success = self._device(json.dumps(received))
                is_success = self._device(send_str, dst_address)
                if is_success: break
                logger.debug(f"retry : {send_str}")
                time.sleep(1) # リトライは時間間隔を設けて実施

        logger.debug(self.__class__.__name__ + ' : unlock')


class XbeeHandlingDirector(object):
    def __init__(self,
                 builder: XbeeHandlingBuilder,
                 dst_address: Optional[AnyStr] = None):
        self._builder = builder
        self._dst_address = dst_address

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        if any(args):
            # 最初の引数のみ使う
            send_str, *_ = args
            logger.debug(f"{self.__class__.__name__} : __call__() received : {send_str}")
            self.handling(send_str, self._dst_address)

    def handling(
            self,
            send_str: Optional[AnyStr],
            dst_address: Optional[AnyStr] = None,
    ) -> None:
        self._builder.handling_thread(send_str=send_str, dst_address=dst_address)

    def device_open(self) -> bool:
        return self._builder.device_open()

    def device_close(self) -> None:
        self._builder.device_close()

    def executor_shutdown(self) -> None:
        self._builder.executor_shutdown()  # ThreadPoolを閉じる


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    import random
    import time
    import json

    # PORT = 'COM7'
    PORT = 'COM8'
    BAUD = 9600

    xb_dev = XbeeSenderDevice(PORT, BAUD)
    xb_hb = XbeeHandlingBuilder(xb_dev)
    xb_dc = XbeeHandlingDirector(xb_hb)

    print('open')
    xb_dc.device_open()

    time.sleep(1)

    rec_dict = {'CC': 10.0, }
    text = json.dumps(rec_dict)

    print('send:', text)
    xb_dc(text)
    time.sleep(1)

    print('send:', text)
    xb_dc(text)
    time.sleep(1)

    print('send:', text)
    xb_dc(text)

    # def koutei():
    #     while True:
    #         kl = [0] * int(20 * (random.random() + 0.5)) \
    #              + [3] * int(3 * (random.random() + 0.5)) \
    #              + [1] * int(5 * (random.random() + 0.5)) \
    #              + [2] * int(15 * (random.random() + 0.5)) \
    #              + [3] * int(3 * (random.random() + 0.5)) \
    #              + [2] * int(10 * (random.random() + 0.5)) \
    #              + [3] * int(3 * (random.random() + 0.5))
    #         yield kl
    #
    #
    # gen = koutei()
    #
    # for k in range(5):
    #     for i in next(gen):
    #         rec_dict = {'state': i}
    #         text = json.dumps(rec_dict)
    #         xb_dc(text)
    #         time.sleep(3)

    # 十分待機しないと送信が完了する前に閉じてしまう
    time.sleep(5)
    xb_dc.device_close()
    xb_dc.executor_shutdown()

    print('closed')
