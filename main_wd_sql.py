import time
from typing import List, AnyStr, Dict, Optional
from logging import getLogger, StreamHandler, FileHandler, DEBUG
from itertools import islice
import copy
import json

import click
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from dev.xbee_router import XbeeSenderDevice
# from th.xbee_director import XbeeHandlingBuilder, XbeeHandlingDirector
from th.xbee_direcotr_nonthread import XbeeHandlingBuilder, XbeeHandlingDirector

from util.sql_tool import SqliteAnalyzer

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
# # handler = FileHandler('dousa_log.txt')
# handler.setLevel(DEBUG) # 出力するログのレベルを設定(デフォルトはNOTSET)
logger.addHandler(handler)


class MyWatchdogHandler(PatternMatchingEventHandler):
    def __init__(self,
                 patterns: List[AnyStr],
                 sql_table: Optional[AnyStr] = None,
                 xbee_dr: XbeeHandlingDirector = None,
                 sql_ana: SqliteAnalyzer = None,
                 ):
        super().__init__(patterns=patterns)
        self._sql_table = sql_table
        # xbee関連に作成したクラスがthreadを使っているのでwithでshutdownさせたい
        self._xbee_director = xbee_dr
        # self._csv_analyzer = csv_ana
        self._sql_analyzer = sql_ana

    # def post_via_xbee(self, data: Dict):
    #     '''
    #     jsonで送ると送信制限容量を超えてしまうので、valueのみを','で連結した文字列としてxbeeで送信
    #     '''
    #     print(data)
    #     value_list = list(data.values())
    #     data_str = ','.join(map(str, value_list))
    #     # data_str = '021/04/19 18:35,もろみ,------,------,2.09,-.--,4.8,蒸留あり(2分40秒)'
    #     # data_str = '日本語'
    #     data_str = data_str[:100]
    #     # print(data_str)
    #     # print(len(data_str.encode('utf-8')))
    #     self._xbee_director(data_str)

    @staticmethod
    def dict_chunks(dic_data: Dict, size: int = 3) -> Optional[List[Dict]]:
        """
        辞書型を指定したサイズに分割
        Parameters
        ----------
        dic_data : 分割対象となる辞書型のデータ
        size : 分割時の最大要素数
        """
        if dic_data is None: return
        it = iter(dic_data)
        for i in range(0, len(dic_data), size):
            yield {k: dic_data[k] for k in islice(it, size)}

    def post_via_xbee_segmented_send(self, dic_data: Dict, interval_msec: int = 500):
        """
        xbeeの送信容量制限を回避するために、self._csv_analyzer.csv_data_keyをキーにして、
        各要素を個別に送信する。

        keyが「'測定結果'」の場合
        dict_data = {'測定結果':'2015/10/02 16:10', 'サンプルNo.', "'019-015", ...}
        分割送信データの例('サンプルNo.'を分割送信している)
        {'2015/10/02 16:10': {'サンプルNo.', "'019-015"}}

        Parameters
        ----------
        dic_data : 送信する元データ
        interval_msec :  分割送信する際の時間間隔
        """
        head_key = self._sql_analyzer.sql_data_key
        head_val = dic_data.get(head_key)
        if head_val is None:
            logger.info(f"The key [{head_key}] is not include in the sent dict.")
            return
        # head_keyと同じ要素は除外する
        _ = dic_data.pop(head_key, None)

        # print(dic_data.keys())
        # print(dic_data.values())

        # 辞書型要素の分割ジェネレータ
        chunks = self.dict_chunks(dic_data, size=1)

        if chunks is not None:
            for d in chunks:
                sent_dic = {head_val: d}
                sent_dic = {"boume":sent_dic}
                # sent_data = json.dumps(sent_dic)
                # ↓こうしたほうがxbeeでの日本語送信が安定するように思う？
                # sent_data = json.dumps(sent_dic, ensure_ascii=False, indent=2)
                sent_data = json.dumps(sent_dic, ensure_ascii=False)
                logger.debug(f"send: {sent_data}")
                self._xbee_director(sent_data)
                # 送信間隔を設けておく
                time.sleep(interval_msec / 1000)

    def check_and_send(self, sql_file: AnyStr, table: Optional[AnyStr]):
        new_elements = self._sql_analyzer.get_new_elements(sql_file, table)

        if new_elements is not None and new_elements:
            # 最後の要素の日時情報でyamlファイルを更新するために要素を事前コピーしておく
            last_elem = copy.copy(new_elements[-1])

            # do something
            for d in new_elements:
                # logger.debug(f"send: {d}")
                self.post_via_xbee_segmented_send(d)
                # time.sleep(30)

            # 最後の要素の日時情報でyamlファイルを更新
            self._sql_analyzer.update_yaml_state(sql_file, last_elem)

    def on_modified(self, event):
        """
        PatternMatchingEventHandlerメソッドのOverRide
        """
        logger.debug(f"{event.src_path} is modified.")
        self.check_and_send(event.src_path, self._sql_table)

    def on_moved(self, event):
        logger.debug(f"{event.src_path} is moved.")

    def on_created(self, event):
        logger.debug(f"{event.src_path} is created.")

    def on_deleted(self, event):
        logger.debug(f"{event.src_path} is deleted.")

    def __enter__(self):
        """
        with 文開始時にコールされる。
        ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
        """
        self._xbee_director.device_open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
        例外が発生しないときはすべて None がセットされる
        """
        self._xbee_director.device_close()
        # self._xbee_director.executor_shutdown()


def folder_watch(
        path: AnyStr,
        extension: AnyStr,
        sql_table: AnyStr,
        xbee_dr: XbeeHandlingDirector,
        sql_ana: SqliteAnalyzer,
):
    with MyWatchdogHandler(["*" + extension], sql_table, xbee_dr, sql_ana) as hd:
        observer = Observer()
        observer.schedule(hd, path, recursive=False)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()


@click.command()
@click.option('--path', '-t', default='./', type=click.Path(exists=True))
@click.option('--extension', '-e', default='.db')
@click.option('--port', '-p', default='COM1')
@click.option('--baud', '-b', default=9600, type=int)
@click.option('--sql_table', '-t', default=None, type=str)
@click.option('--sql_key', '-k', default="id")
@click.option('--sql_codec', '-c', default="utf-8")
def cmd(path, extension, port, baud, sql_table, sql_key, sql_codec):
    xb_dev = XbeeSenderDevice(port, baud)
    xb_hb = XbeeHandlingBuilder(xb_dev)
    xb_dc = XbeeHandlingDirector(xb_hb)

    logger.debug(f"db:{path}/table:{sql_table}/column:{sql_key}")

    sa = SqliteAnalyzer(
        key_column=sql_key,
        codec=sql_codec,
    )
    folder_watch(path, extension, sql_table, xb_dc, sa)


def main():
    cmd()


if __name__ == '__main__':
    '''
    スペース入りの文字列を渡して実行する方法
    python main_wd_sql.py -p COM7 -k No -c CP932
    python main_wd_sql.py -p COM7 -k No -c CP932 -b 115200
    python main_wd_sql.py -p COM7 -e .db -t AlcoholMeasData -k No -c Shift_JIS
    
    「update_test.py」などでutf8の文字情報を格納した場合は下記のようにしないとエラーになると思う
    python main_wd_sql.py -p COM7 -k No -c utf8
    '''
    main()
